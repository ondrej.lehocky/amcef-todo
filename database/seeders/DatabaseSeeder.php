<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Category;
use App\Models\User;
use App\Models\Task;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Category::factory(3)->create();
        User::factory(10)->create();
        Task::factory(50)->create();

        // pivot table seeders
        for($i=0; $i<20; $i++) {
            $user = User::all()->random();
            $task = Task::all()->random();

            $user->sharedTasks()->attach($task->id);
        }
    }
}
