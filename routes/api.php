<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\API\TaskController;
use App\Http\Controllers\API\SharingController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login']);

Route::middleware('auth:sanctum')->group( function () {
    Route::get('/me', [AuthController::class, 'me']);
    
    Route::post('/task/done/{id}', [TaskController::class, 'done']);
    Route::post('/task/restore/{id}', [TaskController::class, 'restore']);
    Route::post('/task/category/{id}', [TaskController::class, 'category']);
    Route::resource('/task', TaskController::class);

    Route::get('/sharing', [SharingController::class, 'index']);
    Route::post('/sharing/{id}', [SharingController::class, 'store']);
    Route::delete('/sharing/{id}', [SharingController::class, 'destroy']);
});