<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;
use Validator;

use App\Models\User;
use App\Models\Task;
use App\Models\Sharing;

class SharingController extends Controller
{
    private $sucess_status = 200;
    private $error_status = 404;

    // [GET] /api/sharing
    // my shared tasks with others users
    public function index(Request $request)
    {
        $tasks = $request->user()->tasks()->get();

        foreach ($tasks as $task)
        {
            $sharings = Task::find($task->id)->sharedUsers()->get();
            if ($sharings->count() > 0)
                $sharing[] = $sharings;
        }

        return response()->json([
            "status" => $this->sucess_status, 
            "success" => true, 
            "message" => "My shared Tasks show successfully.",
            "data" => $sharing
        ]);
    }
    
    // [POST] /api/sharing/{id}
    public function store(Request $request, $id)
    {
        $task = $request->user()->tasks()->find($id);

        if (is_null($task)) {
            return response()->json([
                "status" => $this->error_status, 
                "success" => false, 
                "message" => "Task not found.",
            ], 404);
        }

        $input = $request->all();

        $validator = Validator::make($input, [
            'user_id' => 'required|exists:users,id', // TODO without 'user_id' !!!
        ]);

        if ($validator->fails()){
            return response()->json([
                "status" => $this->error_status, 
                "success" => false, 
                "message" => "Validation Error.",
                "error" => $validator->errors()
            ], 404);
        }

        // $task->sharedUsers()->attach($input['user_id']);
        $task->sharedUsers()->syncWithoutDetaching($input['user_id']);

        return response()->json([
            "status" => $this->sucess_status, 
            "success" => true, 
            "message" => "Sharing Task created successfully.",
            "data" => $task
        ]);
    }

    // [DELETE] /api/sharing/{id}
    public function destroy(Request $request, $id)
    {
        $task = $request->user()->tasks()->find($id);

        if (is_null($task)) {
            return response()->json([
                "status" => $this->error_status, 
                "success" => false, 
                "message" => "Task not found.",
            ], 404);
        }

        $input = $request->all();

        $validator = Validator::make($input, [
            'user_id' => 'required|exists:users,id', // TODO without 'user_id' !!!
        ]);

        if ($validator->fails()){
            return response()->json([
                "status" => $this->error_status, 
                "success" => false, 
                "message" => "Validation Error.",
                "error" => $validator->errors()
            ], 404);
        }

        $task->sharedUsers()->detach($input['user_id']);

        return response()->json([
            "status" => $this->sucess_status, 
            "success" => true, 
            "message" => "Sharing Task unlink successfully.",
            "data" => $task
        ]);
    }
}