<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;
use Validator;

use App\Models\User;
use App\Models\Task;

use App\Event\TaskDone;

class TaskController extends Controller
{
    private $sucess_status = 200;
    private $error_status = 404;

    // [GET] /api/task{?filter=categories|done|shared|all}
    public function index(Request $request)
    {
        // $task = auth('sanctum')->user()->task;

        switch ($request['filter']) {
            case "categories" : 
                $tasks = $request->user()->tasks()->orderBy("category_id")->get();
                break;
            case "done" : 
                $tasks = $request->user()->tasks()->where("done","1")->get();
                break;
            case "shared" :
                // shared tasks with me from others users
                $tasks = $sharing = $request->user()->sharedTasks()->get();
                break;
            case "all" :
            default : $tasks = $request->user()->tasks;
        }

        return response()->json([
            "status" => $this->sucess_status, 
            "success" => true, 
            "message" => "Tasks show successfully.",
            "data" => $tasks
        ]);
    }
    
    // [POST] /api/task
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'title' => 'required',
            'description' => 'required',
        ]);

        if ($validator->fails()){
            return response()->json([
                "status" => $this->error_status, 
                "success" => false, 
                "message" => "Validation Error.",
                "error" => $validator->errors()
            ], 404);
        }

        $task = Task::create($input);

        return response()->json([
            "status" => $this->sucess_status, 
            "success" => true, 
            "message" => "Task created successfully.",
            "data" => $task
        ]);
    }

    // [GET] /api/task/{id}
    public function show(Request $request, $id)
    {
        $task = $request->user()->tasks->find($id);

        if (is_null($task)) {
            return response()->json([
                "status" => $this->error_status, 
                "success" => false, 
                "message" => "Task not found.",
            ], 404);
        }

        return response()->json([
            "status" => $this->sucess_status, 
            "success" => true, 
            "message" => "Task show successfully",
            "data" => $task
        ]);
    }

    // [PUT] /api/task/{id}
    public function update(Request $request, Task $task)
    {
        $task = $request->user()->tasks->find($task->id);

        if (is_null($task)) {
            return response()->json([
                "status" => $this->error_status, 
                "success" => false, 
                "message" => "Task not found.",
            ], 404);
        }

        $input = $request->all();

        $validator = Validator::make($input, [
            'title' => 'required',
            'description' => 'required',
        ]);

        if ($validator->fails()){
            return response()->json([
                "status" => $this->error_status, 
                "success" => false, 
                "message" => "Validation Error.",
                "error" => $validator->errors()
            ], 404);
        }

        $task->title = $input['title'];
        $task->description = $input['description'];
        $task->save();

        return response()->json([
            "status" => $this->sucess_status, 
            "success" => true, 
            "message" => "Task updated successfully.",
            "data" => $task
        ]);
    }

    // [DELETE] /api/task/{id}
    public function destroy(Request $request, Task $task)
    {
        $task = $request->user()->tasks->find($task->id);

        if (is_null($task)) {
            return response()->json([
                "status" => $this->error_status, 
                "success" => false, 
                "message" => "Task not found.",
            ], 404);
        }
        
        $task->delete();

        return response()->json([
            "status" => $this->sucess_status, 
            "success" => true, 
            "message" => "Task deleted successfully.",
            "data" => $task
        ]);
    }

    // [POST] /api/task/restore/{id}
    public function restore(Request $request, $id)
    {
        $user = $request->user();
        $task = Task::onlyTrashed()
                    ->where("user_id", $user->id)
                    ->find($id);

        if (is_null($task)) {
            return response()->json([
                "status" => $this->error_status, 
                "success" => false, 
                "message" => "Task not found.",
            ], 404);
        }
        
        $task->restore();

        return response()->json([
            "status" => $this->sucess_status, 
            "success" => true, 
            "message" => "Task restored successfully.",
            "data" => $task
        ]);
    }

    // [POST] /api/task/done/{id}
    public function done(Request $request, $id)
    {
        $task = $request->user()->tasks->where("done", false)->find($id);

        if (is_null($task)) {
            return response()->json([
                "status" => $this->error_status, 
                "success" => false, 
                "message" => "Task not found.",
            ], 404);
        }

        $task->done = true;
        $task->save();

        $email = $request->user()->email;
        event (new TaskDone($email, $task));

        return response()->json([
            "status" => $this->sucess_status, 
            "success" => true, 
            "message" => "Task done successfully.",
            "data" => $task
        ]);
    }

    // [POST] /api/task/category/{id}
    public function category(Request $request, $id)
    {
        $task = $request->user()->tasks->find($id);

        if (is_null($task)) {
            return response()->json([
                "status" => $this->error_status, 
                "success" => false, 
                "message" => "Task not found.",
            ], 404);
        }

        $input = $request->all();

        $validator = Validator::make($input, [
            'category' => 'required|exists:categories,id',
        ]);

        if ($validator->fails()){
            return response()->json([
                "status" => $this->error_status, 
                "success" => false, 
                "message" => "Validation Error.",
                "error" => $validator->errors()
            ], 404);
        }

        $task->category_id = $input['category'];
        $task->save();

        return response()->json([
            "status" => $this->sucess_status, 
            "success" => true, 
            "message" => "Task category updated successfully.",
            "data" => $task
        ]);
    }
}