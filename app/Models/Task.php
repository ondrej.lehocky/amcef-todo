<?php

namespace App\Models;

use Auth;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'title', 'description', 'done', 'user_id', 'category_id'
    ];

    protected static function booted()
    {
        static::creating(function ($task) {
            if (is_null($task->user_id))
                $task->user_id = Auth::id();
            if (is_null($task->category_id))
                $task->category_id = 1;
        });
    }

    public function owner()
    {
        return $this->belongsTo(User::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function sharedUsers()
    {
        return $this->belongsToMany(User::class)->withTimestamps();
    }
}
