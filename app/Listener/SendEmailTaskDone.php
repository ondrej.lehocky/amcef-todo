<?php

namespace App\Listener;

use App\Event\TaskDone;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Mail;

class SendEmailTaskDone
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Event\TaskDone  $event
     * @return void
     */
    public function handle(TaskDone $event)
    {
        Mail::raw("Hello, your task is DONE.", function($message) use ($event) {
            $message->to($event->email);
            $message->subject("Event: Your Task #" . $event->task->id . " is DONE");
        });
    }
}
